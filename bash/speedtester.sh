#!/bin/bash
# Author: Cody Howard 
# Date: 12/10/2018 
# Purpose: Speedtest Logger  
# Makes use of speedtest-cli.noarch + python36 module termgraph
# Set with cron to run at a frequency of your choosing.
# Recommend every 15 minutes or so. 

# Enter username
user=$(whoami)

# Date Variables
folder_date=$(date "+%m-%Y")
file_date=$(date "+%m-%d-%y")
log_date=$(date "+[%m-%d-%y @ %R:%S]")

# Directory Variables
dir="/home/$user/tools/speedtester"
data_dir="$dir/data/$folder_date"
report_dir="$dir/reports/$folder_date"
misc_dir="$dir/misc/$folder_date"

# File Variables 
misc_file="$misc_dir/$file_date""_Misc.txt"
data_file="$data_dir/$file_date""_Data.txt"
data_temp="$data_dir/$file_date""_Temp.txt"
report_file="$report_dir/$file_date""_Bandwidth_Report.txt"

# Short array for initialization function. 
dirs=("$dir" "$data_dir" "$report_dir" "$misc_dir")


#Functions
initialize() {
	# CHECK FOR DIRECTORY STRUCTURE
	for folder in "${dirs[@]}"; do 
		if ! [[ -d "$folder" ]]; then
			mkdir "$folder" 
		fi
	done 
}

get_data() {
	speedtest --simple > "$misc_file"
	awk  '{ printf ("%s %s\n", $1, $2) }' "$misc_file" | tee -a "$data_file" "$data_temp"
}


build_report() {
	printf '\n%s\n' "$log_date" >> "$report_file"
	/home/"$user"/.local/bin/termgraph "$data_temp" >> "$report_file"
	rm "$data_temp"
}

initialize
get_data
build_report
