#Author: Cody Howard 
#Date: 12/12/2018 
#Purpose: Rewrite speedtest logger in Python
# Need to create monthly log file for data aggregation. 
# Need to add module + pkg check  

# Libs
from datetime import datetime
import subprocess
import os 

# General Variables 
report_data = []
user = os.environ["USER"]
termgraph = (subprocess.check_output(["which","termgraph"]).decode('ascii')).strip('\n')

# Date Variables 
now = datetime.now()
timestamp = now.strftime('[%m-%d-%y @ %H:%M]')
file_date = now.strftime('%m-%d-%y')
fldr_date = now.strftime('%m-%Y')
date_str = "{}\n".format(timestamp)

# Directory Variables 
base_dir    = ( "/home/" + user + "/tools/speedtester" )
data_dir    = ( base_dir + "/data/" + fldr_date )
report_dir  = ( base_dir + "/reports/" + fldr_date )
misc_dir    = ( base_dir + "/misc/" + fldr_date )

# File Variables 
misc_file = ( misc_dir + "/" + file_date + "_Misc.txt" )
data_file = ( data_dir + "/" + file_date + "_Data.txt" )
data_temp = ( data_dir + "/" + file_date + "_Temp.txt" )
report_file = ( report_dir + "/" + file_date + "_Bandwidth_Report.txt")
dirs = [ base_dir , data_dir , report_dir , misc_dir ]

# Directory Check 
for directory in dirs: 
    if not os.path.exists(directory):
        os.makedirs(directory)

# Create Base Array
base_data = ((subprocess.check_output(["speedtest", "--simple"]).decode('ascii')).split('\n'))
for item in base_data:
    report_data.append(item.split(' '))

# Remove Misc Data 
del report_data[3]

# Build Report Data 
for x in range(len(report_data)):
    del report_data[x][2]

# Write Temp File
with open( data_temp ,'w' ) as temp:
    for x,y in report_data:
        temp_str = "{0} {1}\n".format(x,y)
        temp.write(temp_str)

# Write Daily Report 
graph_data = ((subprocess.check_output( [ termgraph, data_temp ]).decode('utf-8')).split('\n'))

with open( report_file ,'a') as report:
    report.write(date_str)
    for line in graph_data:
        rep_str = "{}\n".format(line)
        report.write(rep_str)




