Purpose: Speedtest Logger  

Makes use of speedtest-cli.noarch + python36 module termgraph

Set with cron to run at a frequency of your choosing.

Recommend every 15 minutes or so. 
